import React, { useEffect } from 'react';
import L from 'leaflet' ;

const getMappedChars = (chars) => {
  const mappedChars = chars.reduce((acc, char) => {
    const mappedChar = {
      char,
      coordinates: [char.y, char.x],
      id: char.hero.split(' ').join(''),
    }
    acc.push(mappedChar)
    return acc;
  }, []);
  return mappedChars;
}

export default (props) => {
  // init map
  useEffect(() => {
    const map = L.map('map',{crs: L.CRS.Simple});
    window.map = map

    const bounds = [[0,0], [890, 1280]];
    L.imageOverlay('./img/map.png', bounds).addTo(map);
    map.fitBounds(bounds);

  }, []);

  // Add markers
  useEffect(() => {
    if (props.characters.length === 19) {
      getMappedChars(props.characters).forEach(el => {
        const icon = L.icon({
          iconUrl: `./img/${el.char.house.toLowerCase()}.png`,
          iconSize: [60],
        });
        L.marker(el.coordinates, {
          name: 'MARKER',
          id: el.id,
          title: el.char.hero,
          icon,
        }).bindPopup(el.char.hero).addTo(window.map)
      });
    }    
  }, [props.characters.length])

  // Update markers
  useEffect(() => {
    getMappedChars(props.characters).forEach(el => {
      if (!window.map) return;

      window.map.eachLayer(layer => {
      if (layer.options.name === 'MARKER') {
          if (layer.options.id === el.id) {
            layer.setLatLng(el.coordinates);
          }
        }
      })
    });
  }, [props.characters]);

  return(
    <>Map
      <div id="map"></div>
    </>
  )
}
