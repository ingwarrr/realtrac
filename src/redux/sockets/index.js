
import * as actions from '../actions/actions';

const wsUrl = 'ws://localhost:8080/';

const setupSocket = (dispatch) => {
	const socket = new WebSocket(wsUrl);

	socket.onopen = () => console.log('Connection opened')
	socket.onmessage = (event) => {
		const data = JSON.parse(event.data);
		dispatch(actions.setHero(data));
	}
	return socket;
}

export default setupSocket;
