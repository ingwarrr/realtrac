import * as types from '../actions/types';

const initialState = {
    characters: [],
}

export default function mapReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_HERO:
      const characters =  [...state.characters];
      const newHeroData = action.payload;
      const prevHeroData = characters && characters.find(hero => hero.hero === newHeroData.hero);
      if (prevHeroData) {
        for (const key in prevHeroData) {
          prevHeroData[key] = newHeroData[key];
        }
      }else {
        characters.push(newHeroData);
      }
      return {
        ...state,
        ...{
          characters
        },
      };
    default:
      return state;
  }
}