  
import { take } from 'redux-saga/effects'
import * as types from '../actions/types';

export default function* () {
	yield take(types.SET_HERO)
}
