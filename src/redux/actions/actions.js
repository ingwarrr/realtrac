import * as types from './types';

export const setHero = (data) => ({ type: types.SET_HERO, payload: data });
