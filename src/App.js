import React from 'react';
import Map from './components/map';

import { connect } from 'react-redux';

const mapStateToProps = state => ({characters: state.map.characters});
const ConnectedMap = connect(mapStateToProps)(Map);

function App() {
  return (
    <>
      <ConnectedMap hero={[803.5, 301]}/>
    </>
  );
}

export default App;
